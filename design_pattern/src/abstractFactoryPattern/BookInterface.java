package abstractFactoryPattern;

public interface BookInterface {
	int getPrice();
}
