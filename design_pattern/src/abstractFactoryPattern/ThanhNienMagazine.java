package abstractFactoryPattern;

public class ThanhNienMagazine extends NewspaperAbstract {
	private final int PRICE;

	public ThanhNienMagazine() {
		// TODO Auto-generated constructor stub
		PRICE = 1500;
	}

	@Override
	public int getPrice(int quantity) {
		// TODO Auto-generated method stub
		return quantity * PRICE;
	}

}
