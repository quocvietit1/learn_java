package abstractFactoryPattern;

public abstract class NewspaperAbstract {
	protected int price;
	protected final float DISCOUNT = 0.9f;

	public abstract int getPrice(int quantity);

	public float calculatePrice() {
		if (price > 100000) {
			return price * DISCOUNT;
		}
		return price;
	}

}
