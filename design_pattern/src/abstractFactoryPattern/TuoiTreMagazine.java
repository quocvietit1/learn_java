package abstractFactoryPattern;

public class TuoiTreMagazine extends NewspaperAbstract {
	private final int PRICE;

	public TuoiTreMagazine() {
		// TODO Auto-generated constructor stub
		PRICE = 1000;
	}

	@Override
	public int getPrice(int quantity) {
		return quantity * PRICE;
	}

}
