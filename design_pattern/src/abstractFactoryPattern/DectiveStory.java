package abstractFactoryPattern;

public class DectiveStory implements BookInterface {
	private final int PRICE;
	
	public DectiveStory() {
		PRICE = 10000;
	}
	
	@Override
	public int getPrice() {
		return PRICE;
	}

}
