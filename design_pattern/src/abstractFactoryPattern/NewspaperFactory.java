package abstractFactoryPattern;

public class NewspaperFactory extends LibraryFactory {

	@Override
	public BookInterface getBook(String bookType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NewspaperAbstract getNewspaper(String pulisher) {
		// TODO Auto-generated method stub
		if (pulisher == null) {
			return null;
		}

		if (pulisher.equalsIgnoreCase("ThanhNien")) {
			return new ThanhNienMagazine();
		} else if (pulisher.equalsIgnoreCase("TuoiTre")) {
			return new TuoiTreMagazine();
		}
		return null;
	}

}
