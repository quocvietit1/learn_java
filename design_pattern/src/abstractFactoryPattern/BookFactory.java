package abstractFactoryPattern;

public class BookFactory extends LibraryFactory {

	@Override
	public BookInterface getBook(String bookType) {
		// TODO Auto-generated method stub
		if (bookType == null) {
			return null;
		}

		if (bookType.equalsIgnoreCase("DetectiveStory")) {
			return new DectiveStory();
		} else if (bookType.equalsIgnoreCase("Comic")) {
			return new Comic();
		}
		return null;
	}

	@Override
	public NewspaperAbstract getNewspaper(String pulisher) {
		// TODO Auto-generated method stub
		return null;
	}

}
