package abstractFactoryPattern;

public class LibraryCreator {
	public static LibraryFactory getLibrary(String type) {
		if (type == null) {
			return null;
		}

		if (type.equalsIgnoreCase("Book")) {
			return new BookFactory();
		} else if (type.equalsIgnoreCase("Newspaper")) {
			return new NewspaperFactory();
		}
		return null;
	}
}
