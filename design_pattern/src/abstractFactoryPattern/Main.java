package abstractFactoryPattern;

public class Main {
	public static void main(String[] args) {
		LibraryFactory libraryFactory = LibraryCreator.getLibrary("Book");
		
		//Book
		BookInterface book = libraryFactory.getBook("Comic");
		System.out.println(book.getPrice());
	}
}
