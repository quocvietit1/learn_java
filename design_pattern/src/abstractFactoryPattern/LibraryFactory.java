package abstractFactoryPattern;

public abstract class LibraryFactory {
	public abstract BookInterface getBook(String bookType);
	public abstract NewspaperAbstract getNewspaper(String pulisher);
}
