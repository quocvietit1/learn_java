package factoryPattern;

public abstract class Book {
	protected int price;
	abstract void getPrice();
	
	public int calculateBill(int quantity){
		return quantity*price;
	}
}
