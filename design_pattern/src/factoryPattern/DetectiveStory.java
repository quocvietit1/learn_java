package factoryPattern;

public class DetectiveStory extends Book {

	@Override
	void getPrice() {
		price = 15000;
	}

}
