package factoryPattern;

import java.util.Scanner;

public class GenarateBill {
	private static Scanner sc;

	public static void main(String[] args) {
		GetBookFactory bookFactory = new GetBookFactory();
		sc = new Scanner(System.in);
		
		System.out.printf("Enter book type: ");
		String bookType = sc.nextLine();
		
		System.out.printf("Enter number of book: ");
		int quantity = sc.nextInt();
		
		Book book = bookFactory.getBook(bookType);
		book.getPrice();
		System.out.printf("Price %d of %s: %d", quantity, bookType, book.calculateBill(quantity));
	}
}
