package factoryPattern;

public class GetBookFactory {
	public Book getBook(String bookType){
		if(bookType == null){
			return null;
		}
		
		if(bookType.equalsIgnoreCase("DetectiveStory")){
			return new DetectiveStory();
		}else if (bookType.equalsIgnoreCase("Comic")){
			return new Comic();
		}
		
		return null;
	}
}
