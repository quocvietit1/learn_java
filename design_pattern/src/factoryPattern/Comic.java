package factoryPattern;

public class Comic extends Book {

	@Override
	void getPrice() {
		price = 10000;
	}
	
}
