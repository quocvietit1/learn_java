package dependencyInjection;

public class LibraryImplement implements LibraryInterface {
	private BookInterface book;

	public LibraryImplement(BookInterface book) {
		// TODO Auto-generated constructor stub
		this.book = book;
	}

	@Override
	public String getBookName() {
		// TODO Auto-generated method stub
		return book.getName();
	}

}
