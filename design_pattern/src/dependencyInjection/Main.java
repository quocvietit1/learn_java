package dependencyInjection;

public class Main {
	public static void main(String[] args) {
		LibraryImplement library = (LibraryImplement)new DependencyInjectorImplement().getBook();
		System.out.println(library.getBookName());
	}
}	
