package dependencyInjection;

public interface LibraryInterface {
	public String getBookName();
}
