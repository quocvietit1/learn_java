Giảm sự phụ thuộc lẫn nhau giữa các class. Dễ bào trì và nâng cấp.

UML:
-  Giữa 2 đối tượng có sự phụ thuộc tạo interface để gọi và sử dụng một lớp injection để tiêm đối tượng cần sử dụng vào.
-  Khi muốn thay đổi đối tượng chỉ cần sữa trong injection và tạo đối tượng mới để tiêm vào